public interface UnaryFunction {
    @FunctionalInterface
    public interface Function<T1, R> {
        R binary (T1 var1);
    }
}
