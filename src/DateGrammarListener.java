// Generated from C:/Users/kryst/IdeaProjects/date-grammar-antlr/src\DateGrammar.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link DateGrammarParser}.
 */
public interface DateGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link DateGrammarParser#date}.
	 * @param ctx the parse tree
	 */
	void enterDate(DateGrammarParser.DateContext ctx);
	/**
	 * Exit a parse tree produced by {@link DateGrammarParser#date}.
	 * @param ctx the parse tree
	 */
	void exitDate(DateGrammarParser.DateContext ctx);
	/**
	 * Enter a parse tree produced by {@link DateGrammarParser#dateTime}.
	 * @param ctx the parse tree
	 */
	void enterDateTime(DateGrammarParser.DateTimeContext ctx);
	/**
	 * Exit a parse tree produced by {@link DateGrammarParser#dateTime}.
	 * @param ctx the parse tree
	 */
	void exitDateTime(DateGrammarParser.DateTimeContext ctx);
	/**
	 * Enter a parse tree produced by {@link DateGrammarParser#timeSpan}.
	 * @param ctx the parse tree
	 */
	void enterTimeSpan(DateGrammarParser.TimeSpanContext ctx);
	/**
	 * Exit a parse tree produced by {@link DateGrammarParser#timeSpan}.
	 * @param ctx the parse tree
	 */
	void exitTimeSpan(DateGrammarParser.TimeSpanContext ctx);
	/**
	 * Enter a parse tree produced by {@link DateGrammarParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(DateGrammarParser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DateGrammarParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(DateGrammarParser.OperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DateGrammarParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary(DateGrammarParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link DateGrammarParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary(DateGrammarParser.UnaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link DateGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(DateGrammarParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DateGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(DateGrammarParser.ExpressionContext ctx);
}