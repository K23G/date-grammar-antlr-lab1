grammar DateGrammar;

fragment SINGLE_SPACE   : '\u0020'; // ' '
fragment TABULATION     : '\u0009'; // '\t'
fragment LINE_FEED      : '\u000A'; // '\n'
fragment CARRAIGE_RETURN: '\u000D'; // '\r'
fragment DOT            : '.';
fragment SLASH            : '/';
fragment BACKSLASH      : '\\';
fragment COLON          : ':';
fragment DASH           : '-';
fragment D              :[0-9];



WhiteSpace: ( SINGLE_SPACE | TABULATION | CARRAIGE_RETURN | SLASH | DOT)+ -> skip;

TIMESPAN: T Y COLON T M COLON T DY COLON T H COLON T MIN COLON T S
        |T Y COLON T M COLON T H COLON T S
        | T Y COLON T M COLON T DY COLON T H COLON T MIN
        | T Y COLON T M COLON T DY COLON T MIN
        | T Y COLON T M COLON T H COLON T MIN
        | T Y COLON T M COLON T DY COLON T H
        | T Y COLON T M COLON T DY
        | T Y COLON T M
        | T Y
        | T Y COLON T DY COLON T H COLON T MIN COLON T S
        | T Y COLON T M COLON T DY COLON T S
        | T Y COLON T DY COLON T H COLON T MIN
        | T Y COLON T DY COLON T H COLON T S
        | T Y COLON T H COLON T MIN COLON T S
        |T Y COLON T DY COLON T MIN COLON T S
        | T Y COLON T H COLON T MIN
        |T Y COLON T M COLON T H COLON T MIN COLON T S
        |T Y COLON T M COLON T H
        |T Y COLON T M COLON T MIN COLON T S
        |T Y COLON T M COLON T MIN
        |T Y COLON T M COLON T DY COLON T MIN COLON T S
        |T Y COLON T MIN COLON T S
        |T Y COLON T M COLON T DY COLON T H COLON T S
        |T Y COLON T H COLON T S
        |T Y COLON T M COLON T DY COLON T S
        |T Y COLON T DY COLON T H
        |T Y COLON T M COLON T S
        |T Y COLON T S
        |T Y COLON T M COLON T DY COLON T MIN
        |T Y COLON T MIN
        |T Y COLON T H
        |T Y COLON T DY
        |T M COLON T DY COLON T H COLON T MIN COLON T S
        |T M COLON T DY COLON T MIN COLON T S
        |T M COLON T DY COLON T H COLON T S
        |T M COLON T DY COLON T H
        |T M COLON T DY COLON T MIN
        |T M COLON T DY COLON T S
        |T M COLON T H COLON T MIN COLON T S
        |T M COLON T MIN COLON T S
        |T M COLON T H COLON T S
        |T M COLON T DY COLON T H COLON T MIN
        |T M COLON T H COLON T MIN
        |T M COLON T DY
        |T M
        |T M COLON T S
        |T M COLON T MIN
        |T M COLON T H
        |T DY COLON T H COLON T MIN COLON T S
        |T DY COLON T H COLON T S
        |T DY COLON T MIN COLON T S
        |T DY COLON T H COLON T MIN
        |T DY COLON T H
        |T DY
        |T DY COLON T S
        |T DY COLON T MIN
        |T H COLON T MIN COLON T S
        |T H COLON T MIN
        |T H
        |T H COLON T S
        |T MIN COLON T S
        |T MIN
        |T S
        ;


T:        D
        | D D
        | D D D
        | D D D D
        ;

Y: 'Y';
M: 'M';
DY: 'D';
H: 'h';
MIN: 'm';
S: 's';

DATETIME: DATE 'T' TIME;

DATE : (LONGMONTHDAY DOT LONGMONTH DOT YEAR)
       |(SHORTMONTHDAY DOT SHORTMONTH DOT YEAR)
       |(SHORTFEBDAY DOT FEB DOT YEAR)
       |(LONGFEBDAY DOT FEB DOT YEAR)
       |(LONGMONTHDAY SLASH LONGMONTH SLASH YEAR)
       |(SHORTMONTHDAY SLASH SHORTMONTH SLASH YEAR)
       |(SHORTFEBDAY SLASH FEB SLASH YEAR)
       |(LONGFEBDAY SLASH FEB SLASH YEAR)
       |(LONGMONTHDAY DASH LONGMONTH DASH YEAR)
       |(SHORTMONTHDAY DASH SHORTMONTH DASH YEAR)
       |(SHORTFEBDAY DASH FEB DASH YEAR)
       |(LONGFEBDAY DASH FEB DASH YEAR)
       | (YEAR DOT LONGMONTH DOT LONGMONTHDAY)
       | (YEAR DOT SHORTMONTH DOT SHORTMONTHDAY)
       | (YEAR DOT FEB DOT SHORTFEBDAY)
       | (YEAR DOT FEB DOT LONGFEBDAY)
       | (YEAR DASH LONGMONTH DASH LONGMONTHDAY)
       | (YEAR DASH SHORTMONTH DASH SHORTMONTHDAY)
       | (YEAR DASH FEB DASH SHORTFEBDAY)
       | (YEAR DASH FEB DASH LONGFEBDAY)
       | (YEAR SLASH LONGMONTH SLASH LONGMONTHDAY)
       | (YEAR SLASH SHORTMONTH SLASH SHORTMONTHDAY)
       | (YEAR SLASH FEB SLASH SHORTFEBDAY)
       | (YEAR SLASH FEB SLASH LONGFEBDAY);


TIME: HOUR COLON MINUTE COLON SECOND;


HOUR: '0'D | '1'D | '2'[0-3];
MINUTE: [0-5]D;
SECOND: [0-5]D;

YEAR: D D D D;

MONTH : LONGMONTH | SHORTMONTH | FEB ;

LONGMONTH : JAN | MAR | MAY | JUL | AUG | OCT | DEC ;
SHORTMONTH : APR | JUN | SEP | NOV ;



DAY : LONGMONTHDAY
    | SHORTMONTHDAY
    | LONGFEBDAY
    | SHORTFEBDAY
    ;



JAN : 'JAN' | 'jan' | 'STY' | 'sty' | 'January' | 'Styczen' | '01' | 'I';
FEB : 'FEB' | 'feb' | 'LUT' | 'lut' | 'February' | 'Luty' | '02' | 'II';
MAR : 'MAR' | 'mar' | 'March' | 'Marzec' | '03' | 'III';
APR : 'APR' | 'apr' | 'KWI' | 'kwi' | 'April' | 'Kwiecien' | '04' | 'IV';
MAY : 'MAY' | 'may' | 'MAJ' | 'maj' | 'May' | 'Maj' | '05' | 'V';
JUN : 'JUN' | 'jun' | 'CZE' | 'cze' | 'June' | 'Czerwiec' | '06' | 'VI';
JUL : 'JUL' | 'jul' | 'LIP' | 'lip' | 'July' | 'Lipiec' | '07' | 'VII';
AUG : 'AUG' | 'aug' | 'SIE' | 'sie' | 'August' | 'Sierpien' | '08' | 'VIII';
SEP : 'SEP' | 'sep' | 'WRZ' | 'wrz' | 'September' | 'Wrzesien' | '09' | 'IX';
OCT : 'OCT' | 'oct' | 'PAZ' | 'paz' | 'October' | 'Pazdziernik' | '10' | 'X';
NOV : 'NOV' | 'nov' | 'LIS' | 'lis' | 'November' | 'Listopad' | '11' | 'XI';
DEC : 'DEC' | 'dec' | 'GRU' | 'gru' | 'December' | 'Grudzien' | '12' | 'XII';

LONGMONTHDAY  : '0'[1-9] | [1-2]D | '3'[0-1];
SHORTMONTHDAY : '0'[1-9] | [1-2]D | '30';
SHORTFEBDAY   : '0'[1-9] | [1-2]D;
LONGFEBDAY    : '0'[1-9] | [1-2][0-8];


SEPARATOR : [/\\\-] ;

CH         : '*';
SINCE      : '&';
PLUS       : '+';
MINUS      : '|';


date          : DATE;

dateTime      : DATETIME;

timeSpan      : TIMESPAN;

operation : op=( PLUS
               | MINUS
               )

               (( date | dateTime | timeSpan | unary | operation ) ( timeSpan | operation )
                  |    ( timeSpan | operation ) ( date | dateTime | timeSpan | unary | operation )
                  |    ( date | dateTime | timeSpan | unary | operation) (date | dateTime | timeSpan | unary | operation )
                )
          | '(' operation ')'
          ;

unary     : u = (
                SINCE
                |CH
                )
                 ((date|dateTime|operation)
                  | '(' operation ')')
          ;

expression: operation
          | unary
          | date
          | dateTime
          | timeSpan
          ;


