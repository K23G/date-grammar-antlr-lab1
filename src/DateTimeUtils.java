import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class DateTimeUtils {
    private static final String DOT = ".";
    private static final String SLASH = "/";
    private static final String COLON = ":";
    private static final String DASH = "-";
    private static final char YEAR = 'Y';
    private static final char MONTH = 'M';
    private static final char DAY = 'D';
    private static final char HOUR = 'h';
    private static final char MINUTE = 'm';
    private static final char SECOND = 's';


    private static int[] takeTimeSpan(String timeSpan) {
        String[] time = timeSpan.split(":");
        int[] date = new int[6];

        for (int i = 0; i < 6; i++) {
            date[i] = 0;
        }

        for (String s : time) {
            switch (s.substring(s.length() - 1)) {
                case "Y":
                    date[0] = Integer.parseInt(s.substring(0, s.length() - 1));
                    break;
                case "M":
                    date[1] = Integer.parseInt(s.substring(0, s.length() - 1));
                    break;
                case "D":
                    date[2] = Integer.parseInt(s.substring(0, s.length() - 1));
                    break;
                case "h":
                    date[3] = Integer.parseInt(s.substring(0, s.length() - 1));
                    break;
                case "m":
                    date[4] = Integer.parseInt(s.substring(0, s.length() - 1));
                    break;
                case "s":
                    date[5] = Integer.parseInt(s.substring(0, s.length() - 1));
                    break;
            }
        }

        return date;
    }

    public static LocalDate resolveDate(String date) {
        LocalDate localDate = null;
        if (date.contains(DOT)) {
            String[] arr = date.split("\\.");
            localDate = parseArrayToLocalDate(arr);
        }
        if (date.contains(SLASH)) {
            String[] arr = date.split("/");
            localDate = parseArrayToLocalDate(arr);
        }
        if (date.contains(DASH)) {
            String[] arr = date.split(DASH);
            localDate = parseArrayToLocalDate(arr);
        }
        return localDate;
    }

    private static LocalDate parseArrayToLocalDate(String[] arr) {
        if (arr[0].length() == 4) {
            LocalDate checkLD = LocalDate.of(Integer.parseInt(arr[0]), 1, 1);
            if (!checkLD.isLeapYear()) {
                if (Integer.parseInt(DateTimeUtils.stringToMonthConv.get(arr[1])) == 2) {
                    if (Integer.parseInt(arr[2]) > 28) {
                        throw new IllegalArgumentException();
                    }
                }
            }
            return LocalDate.of(Integer.parseInt(arr[0]), Integer.parseInt(DateTimeUtils.stringToMonthConv.get(arr[1])), Integer.parseInt(arr[2]));
        } else if (arr[2].length() == 4) {
            LocalDate checkLD = LocalDate.of(Integer.parseInt(arr[2]), 1, 1);
            if (!checkLD.isLeapYear()) {
                if (Integer.parseInt(DateTimeUtils.stringToMonthConv.get(arr[1])) == 2) {
                    if (Integer.parseInt(arr[0]) > 28) {
                        throw new IllegalArgumentException();
                    }
                }
            }
            return LocalDate.of(Integer.parseInt(arr[2]), Integer.parseInt(DateTimeUtils.stringToMonthConv.get(arr[1])), Integer.parseInt(arr[0]));
        } else throw new IllegalArgumentException();
    }

    public static LocalDateTime resolveDateTime(String dateTime) {
        String month = DateTimeUtils.stringToMonthConv.get(dateTime.split("[.]|[/]|[-]")[1]);
        String newDT = dateTime.replace("[.][a-zA-Z0-9]+[.]", ("." + month + "."));
        String[] dT = newDT.split("T");
        String date = dT[0];
        String time = dT[1];
        String[] arr = time.split(COLON);
        LocalDate localDate = resolveDate(date);
        LocalDateTime localDateTime = localDate.atStartOfDay();
        localDateTime = localDateTime.plusHours(Long.parseLong(arr[0]));
        localDateTime = localDateTime.plusMinutes(Long.parseLong(arr[1]));
        localDateTime = localDateTime.plusSeconds(Long.parseLong(arr[2]));
        return localDateTime;
    }

    public static LocalDateTime addTimeStamp(LocalDate date, String timeStamp) {
        int[] time = takeTimeSpan(timeStamp);
        LocalDateTime dateTime = date.atStartOfDay();

        dateTime = dateTime.plusYears(time[0]);
        dateTime = dateTime.plusMonths(time[1]);
        dateTime = dateTime.plusDays(time[2]);
        dateTime = dateTime.plusHours(time[3]);
        dateTime = dateTime.plusMinutes(time[4]);
        dateTime = dateTime.plusSeconds(time[5]);

        return dateTime;
    }

    public static LocalDateTime addTimeStamp(LocalDateTime dateTime, String timeStamp) {
        int[] time = takeTimeSpan(timeStamp);

        dateTime = dateTime.plusYears(time[0]);
        dateTime = dateTime.plusMonths(time[1]);
        dateTime = dateTime.plusDays(time[2]);
        dateTime = dateTime.plusHours(time[3]);
        dateTime = dateTime.plusMinutes(time[4]);
        dateTime = dateTime.plusSeconds(time[5]);

        return dateTime;
    }

    public static String addTimeStamp(String timeStamp1, String timeStamp2) {

        int[] time1 = takeTimeSpan(timeStamp1);
        int[] time2 = takeTimeSpan(timeStamp2);
        StringBuilder sb = new StringBuilder();
        String[] units = {"Y", "M", "D", "h", "m", "s"};
        String result = "";

        for (int i = 5; i >= 0; i--) {
            time1[i] += time2[i];
        }

        for (int i = 0; i < 6; i++) {
            if (time1[i] != 0) {
                sb.append(time1[i]);
                sb.append(units[i]);
                sb.append(":");
            }
        }

        result = sb.toString();
        return result.substring(0, result.length() - 1);
    }

    public static char symbol(int a) {
        switch (a) {
            case 1:
                return 'Y';
            case 2:
                return 'M';
            case 3:
                return 'D';
            case 4:
                return 'h';
            case 5:
                return 'm';
            case 6:
                return 's';
            default:
                throw new IllegalArgumentException();
        }
    }

    public static int cast(int a[], int unit) {
        int t = 0;
        switch (unit) {
            case 0:
                t = a[0] * 1;
                break;
            case 1:
                t += a[0] * 12;
                t += a[1];
                //to months
                break;
            case 2:
                t += a[0] * 365;
                t += a[1] * 30;
                t += a[2];
                //to days
                break;
            case 3:
                t += a[0] * 365 * 24;
                t += a[1] * 30 * 24;
                t += a[2] * 24;
                t += a[3];
                //to hours
                break;
            case 4:
                t += a[0] * 365 * 24 * 60;
                t += a[1] * 30 * 24 * 60;
                t += a[2] * 24 * 60;
                t += a[3] * 60;
                t += a[4];
                //to minutes
                break;
            case 5:
                t += a[0] * 365 * 24 * 60 * 60;
                t += a[1] * 30 * 24 * 60 * 60;
                t += a[2] * 24 * 60 * 60;
                t += a[3] * 60 * 60;
                t += a[4] * 60;
                t += a[5];//to seconds
                break;
            default:
                throw new IllegalArgumentException();
        }
        return t;
    }

    public static String dif(LocalDate after, LocalDate before) {
        return after.toEpochDay() - before.toEpochDay() + "D";
    }

    public static String dif(LocalDateTime after, LocalDateTime before) {
        return before.until(after, ChronoUnit.SECONDS) + "s";
    }

    public static LocalDateTime subTimeStamp(LocalDate date, String timeStamp) {
        int[] time = takeTimeSpan(timeStamp);
        LocalDateTime dateTime = date.atStartOfDay();

        dateTime = dateTime.minusYears(time[0]);
        dateTime = dateTime.minusMonths(time[1]);
        dateTime = dateTime.minusDays(time[2]);
        dateTime = dateTime.minusHours(time[3]);
        dateTime = dateTime.minusMinutes(time[4]);
        dateTime = dateTime.minusSeconds(time[5]);

        return dateTime;
    }

    public static LocalDateTime subTimeStamp(LocalDateTime dateTime, String timeStamp) {

        int[] time = takeTimeSpan(timeStamp);

        dateTime = dateTime.minusYears(time[0]);
        dateTime = dateTime.minusMonths(time[1]);
        dateTime = dateTime.minusDays(time[2]);
        dateTime = dateTime.minusHours(time[3]);
        dateTime = dateTime.minusMinutes(time[4]);
        dateTime = dateTime.minusSeconds(time[5]);

        return dateTime;
    }

    public static String subTimeStamp(String timeStamp1, String timeStamp2) {

        int[] time1 = takeTimeSpan(timeStamp1);
        int[] time2 = takeTimeSpan(timeStamp2);
        StringBuilder sb = new StringBuilder();
        String[] units = {"Y", "M", "D", "h", "m", "s"};
        String result = "";
        int min = 0;
        int t1 = 0;
        int t2 = 0;

        for (int i = 1; i < 6; i++) {
            if (time1[i] != 0 || time2[i] != 0) {
                min = i;
            }
        }

        t1 = cast(time1, min);
        t2 = cast(time2, min);

        sb.append(Math.abs(t1 - t2));
        sb.append(units[min]);
        result = sb.toString();
        return result;
    }

    static Map<String, String> stringToMonthConv = new HashMap<>();

    public static void fillMap() {
        stringToMonthConv.put("JAN", "01");
        stringToMonthConv.put("jan", "01");
        stringToMonthConv.put("STY", "01");
        stringToMonthConv.put("sty", "01");
        stringToMonthConv.put("January", "01");
        stringToMonthConv.put("Styczen", "01");
        stringToMonthConv.put("01", "01");
        stringToMonthConv.put("I", "01");

        stringToMonthConv.put("FEB", "02");
        stringToMonthConv.put("feb", "02");
        stringToMonthConv.put("LUT", "02");
        stringToMonthConv.put("lut", "02");
        stringToMonthConv.put("February", "02");
        stringToMonthConv.put("Luty", "02");
        stringToMonthConv.put("02", "02");
        stringToMonthConv.put("II", "02");

        stringToMonthConv.put("MAR", "03");
        stringToMonthConv.put("mar", "03");
        stringToMonthConv.put("March", "03");
        stringToMonthConv.put("Marzec", "03");
        stringToMonthConv.put("03", "03");
        stringToMonthConv.put("III", "03");

        stringToMonthConv.put("APR", "04");
        stringToMonthConv.put("apr", "04");
        stringToMonthConv.put("KWI", "04");
        stringToMonthConv.put("kwi", "04");
        stringToMonthConv.put("April", "04");
        stringToMonthConv.put("Kwiecien", "04");
        stringToMonthConv.put("04", "04");
        stringToMonthConv.put("IV", "04");

        stringToMonthConv.put("MAY", "05");
        stringToMonthConv.put("may", "05");
        stringToMonthConv.put("MAJ", "05");
        stringToMonthConv.put("maj", "05");
        stringToMonthConv.put("May", "05");
        stringToMonthConv.put("Maj", "05");
        stringToMonthConv.put("05", "05");
        stringToMonthConv.put("V", "05");

        stringToMonthConv.put("JUN", "06");
        stringToMonthConv.put("jun", "06");
        stringToMonthConv.put("CZE", "06");
        stringToMonthConv.put("cze", "06");
        stringToMonthConv.put("June", "06");
        stringToMonthConv.put("Czerwiec", "06");
        stringToMonthConv.put("06", "06");
        stringToMonthConv.put("VI", "06");

        stringToMonthConv.put("JUL", "07");
        stringToMonthConv.put("jul", "07");
        stringToMonthConv.put("LIP", "07");
        stringToMonthConv.put("lip", "07");
        stringToMonthConv.put("July", "07");
        stringToMonthConv.put("Lipiec", "07");
        stringToMonthConv.put("07", "07");
        stringToMonthConv.put("VII", "07");

        stringToMonthConv.put("AUG", "08");
        stringToMonthConv.put("aug", "08");
        stringToMonthConv.put("SIE", "08");
        stringToMonthConv.put("sie", "08");
        stringToMonthConv.put("August", "08");
        stringToMonthConv.put("Sierpien", "08");
        stringToMonthConv.put("08", "08");
        stringToMonthConv.put("VIII", "08");

        stringToMonthConv.put("SEP", "09");
        stringToMonthConv.put("sep", "09");
        stringToMonthConv.put("WRZ", "09");
        stringToMonthConv.put("wrz", "09");
        stringToMonthConv.put("September", "09");
        stringToMonthConv.put("Wrzesien", "09");
        stringToMonthConv.put("09", "09");
        stringToMonthConv.put("IX", "09");

        stringToMonthConv.put("OCT", "10");
        stringToMonthConv.put("oct", "10");
        stringToMonthConv.put("PAZ", "10");
        stringToMonthConv.put("paz", "10");
        stringToMonthConv.put("October", "10");
        stringToMonthConv.put("Pazdziernik", "10");
        stringToMonthConv.put("10", "10");
        stringToMonthConv.put("X", "10");

        stringToMonthConv.put("NOV", "11");
        stringToMonthConv.put("nov", "11");
        stringToMonthConv.put("LIS", "11");
        stringToMonthConv.put("lis", "11");
        stringToMonthConv.put("November", "11");
        stringToMonthConv.put("Listopad", "11");
        stringToMonthConv.put("11", "11");
        stringToMonthConv.put("XI", "11");

        stringToMonthConv.put("DEC", "12");
        stringToMonthConv.put("dec", "12");
        stringToMonthConv.put("GRU", "12");
        stringToMonthConv.put("gru", "12");
        stringToMonthConv.put("December", "12");
        stringToMonthConv.put("Grudzien", "12");
        stringToMonthConv.put("12", "12");
        stringToMonthConv.put("XII", "12");
    }
}