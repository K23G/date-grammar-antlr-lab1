import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;


public class DateGrammarVisitorImpl extends DateGrammarBaseVisitor {

    public Object plus(Object a, Object b){
        if (a instanceof LocalDate && b instanceof String) {
            LocalDateTime tmp = DateTimeUtils.addTimeStamp((LocalDate) a, (String) b);
            if (tmp.getHour() == 0 && tmp.getMinute() == 0 && tmp.getSecond() == 0)
                return tmp.toLocalDate();
            return tmp;
        }

        if (a instanceof String && b instanceof LocalDate) {
            LocalDateTime tmp = DateTimeUtils.addTimeStamp((LocalDate) b, (String) a);
            if (tmp.getHour() == 0 && tmp.getMinute() == 0 && tmp.getSecond() == 0)
                return tmp.toLocalDate();
            return tmp;
        }

        if (a instanceof LocalDateTime && b instanceof String) {
            return DateTimeUtils.addTimeStamp((LocalDateTime) a, (String) b);
        }

        if((a instanceof LocalDateTime && b instanceof LocalDateTime) || (a instanceof LocalDateTime && b instanceof LocalDate)
            || (a instanceof LocalDate && b instanceof LocalDateTime) || (a instanceof LocalDate && b instanceof LocalDate)) {
            throw new IllegalArgumentException();
        }

        if (a instanceof String && b instanceof LocalDateTime) {
            return DateTimeUtils.addTimeStamp((LocalDateTime) b, (String) a);
        }

        if (a instanceof String && b instanceof String) {
            return DateTimeUtils.addTimeStamp((String) a, (String) b);
        }

        return 1;
    }

    public Object minus(Object a, Object b){
        if (a instanceof LocalDate && b instanceof LocalDate) {
            if (((LocalDate) a).isBefore((LocalDate) b))
                return DateTimeUtils.dif((LocalDate) b, (LocalDate) a);
            else
                return DateTimeUtils.dif((LocalDate) a, (LocalDate) b);
        }

        if (a instanceof LocalDateTime && b instanceof LocalDateTime) {
            if (((LocalDateTime) a).isBefore((LocalDateTime) b))
                return DateTimeUtils.dif((LocalDateTime) b, (LocalDateTime) a);
            else
                return DateTimeUtils.dif((LocalDateTime) a, (LocalDateTime) b);
        }

        if (((a instanceof LocalDate && b instanceof LocalDateTime)
                || (a instanceof LocalDateTime && b instanceof LocalDate))) {

            LocalDateTime tmpA = null;
            LocalDateTime tmpB = null;
            if (a instanceof LocalDate) {
                tmpA = ((LocalDate) a).atStartOfDay();
                tmpB = (LocalDateTime) b;
            }
            if (b instanceof LocalDate) {
                tmpA = ((LocalDate) b).atStartOfDay();
                tmpB = (LocalDateTime) a;
            }

            if ((tmpA).isBefore(tmpB))
                return DateTimeUtils.dif(tmpB, tmpA);
            else
                return DateTimeUtils.dif(tmpA, tmpB);

        }


        if (a instanceof LocalDate && b instanceof String) {
            LocalDateTime tmp = DateTimeUtils.subTimeStamp((LocalDate) a, (String) b);
            if (tmp.getHour() == 0 && tmp.getMinute() == 0 && tmp.getSecond() == 0)
                return tmp.toLocalDate();
            return tmp;
        }

        if (a instanceof String && b instanceof LocalDate) {
            throw new IllegalArgumentException();
        }

        if (a instanceof LocalDateTime && b instanceof String) {
            return DateTimeUtils.subTimeStamp((LocalDateTime) a, (String) b);
        }

        if (a instanceof String && b instanceof LocalDateTime) {
            throw new IllegalArgumentException();
        }

        if (a instanceof String && b instanceof String) {
            return DateTimeUtils.subTimeStamp((String) b, (String) a);
        }

        return 1;
    }

    @Override
    public Object visitDateTime(DateGrammarParser.DateTimeContext ctx) {
        return DateTimeUtils.resolveDateTime(ctx.getText());
    }

    @Override
    public Object visitDate(DateGrammarParser.DateContext ctx) {
        return DateTimeUtils.resolveDate(ctx.getText());
    }

    @Override
    public Object visitTimeSpan(DateGrammarParser.TimeSpanContext ctx) {
        return ctx.getText();
    }

    @Override
    public Object visitOperation(DateGrammarParser.OperationContext ctx) {
        if (null == ctx.op)
            return visit(ctx.operation(0));

        Function<Object, Object, Object> operand;

        switch (ctx.op.getType()) {
            case DateGrammarParser.PLUS:
                operand = (a, b) -> plus(a , b);
                break;

            case DateGrammarParser.MINUS:
                operand = (a, b) -> minus(a, b);
                break;
            default:
                throw new IllegalArgumentException();
        }



        Object left = visit(ctx.getChild(1));
        Object right = visit(ctx.getChild(2));
        return operand.binary(left, right);
    }

    @Override
    public Object visitUnary(DateGrammarParser.UnaryContext ctx) {

        UnaryFunction.Function<Object, Object> operand;

        switch (ctx.u.getType()) {
            case DateGrammarParser.SINCE:
            LocalDate grunwald = LocalDate.of(1410, 7, 15);

            operand = (a) ->
            {
                if(a instanceof String) {
                    throw new IllegalArgumentException();
                }
                else return minus(grunwald, a);
            };
            break;

            case DateGrammarParser.CH:
                operand = (a) -> {
                    LocalDate christmas = LocalDate.of(0, 12, 24);
                    LocalDate tmpD;
                    LocalDateTime tmpDT;

                    if(a instanceof LocalDate){
                        tmpD = (LocalDate) a;
                        int year = tmpD.getYear();
                        christmas = christmas.plusYears(year);

                        if(tmpD.isAfter(christmas)){
                           christmas = christmas.plusYears(1);
                        }
                        return minus(christmas, tmpD);

                    }else if(a instanceof LocalDateTime){
                        tmpDT = (LocalDateTime) a;
                        christmas = christmas.plusYears(tmpDT.getYear());

                        if(tmpDT.isAfter(christmas.atStartOfDay())){
                            christmas = christmas.plusYears(1);
                        }
                        return minus(christmas, tmpDT);
                    } else if(a instanceof String) {
                        throw new IllegalArgumentException();
                    }

                    return 1;
                };
                break;
            default:
                throw new IllegalArgumentException();
        }

        Object a = visit(ctx.getChild(1));
        return operand.binary(a);
    }

    @Override
    public Object visitExpression(DateGrammarParser.ExpressionContext ctx) {
        return super.visitExpression(ctx);
    }
}
